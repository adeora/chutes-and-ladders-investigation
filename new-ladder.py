import random
import csv

"""
AUTHOR: ABHIMANYU DEORA
LICENSE: CC Attribution-NonCommercial-NoDerivatives 4.0 International
DESCRIPTION: Tests all the possible locations for a new ladder - can easily be modified for a new chute
"""

#all the spaces a chute or ladder can start/end on
allowedSpaces = [2, 3, 5, 7, 8, 10, 12, 13, 15, 17, 18, 20, 22, 23, 25, 27, 29, 30, 32, 33, 34, 35, 37, 39, 40, 41, 43, 45, 46, 48, 50, 52, 54, 55, 57, 
58, 59,  61, 63, 65, 66, 68, 69, 70, 72, 74, 76, 77, 79, 82, 83, 85, 86, 88, 89, 90, 92, 94, 96, 97, 99]

#data record list - will be list of dictionaries
dataRecord = []

for startSpace in allowedSpaces:
	print(startSpace)
	#print("\n")

	for endSpace in allowedSpaces:
		if not endSpace <= startSpace:
			print(endSpace)

			##NOW, PLAY THE GAME!!

			spinList = []
			iterations = 0
			newChuteHit = 0
			while (iterations < 10000):
				currentSpace = 0
				spins = 0
				#print(currentSpace)
				#print(spins)
				#print("\n")
				#print("Iteration: %s \n"%(iterations))

				while (currentSpace != 100):
					x = random.randint(1,6)
					#print(x)
					
					if currentSpace + x <= 100:
					    currentSpace += x

					if currentSpace == startSpace:
						currentSpace = endSpace
						newChuteHit += 1
						#print("Hit new Chute!")
					    
					if currentSpace == 1:
					    currentSpace = 38
					if currentSpace == 4:
					    currentSpace = 14
					if currentSpace == 9:
					    currentSpace = 31
					if currentSpace == 21:
					    currentSpace = 42
					    
					if currentSpace == 28:
					    currentSpace = 84
					    
					if currentSpace == 36:
					    currentSpace = 44
					    
					if currentSpace == 47:
					    currentSpace = 26
					    
					if currentSpace == 49:
					    currentSpace = 11
					    
					if currentSpace == 51:
					    currentSpace = 67
					    
					if currentSpace == 56:
					    currentSpace = 53
					    
					if currentSpace == 62:
					    currentSpace = 19
					    
					if currentSpace == 64:
					    currentSpace = 60
					    
					if currentSpace == 71:
					    currentSpace = 91
					    
					if currentSpace == 80:
					    currentSpace = 100
					    
					if currentSpace == 87:
					    currentSpace = 24
					    
					if currentSpace == 93:
					    currentSpace = 73
					    
					if currentSpace == 95:
					    currentSpace = 75
					    
					if currentSpace == 98:
					    currentSpace = 78
					    
					spins += 1
					#print("Current Space: %s"%(currentSpace))
					#print("Iteration #: %s" %(spins))
					#print("\n")
				spinList.append(spins)
				iterations += 1

			placeholdDict = {}
			placeholdDict['startSpace'] = startSpace
			placeholdDict['endSpace'] = endSpace
			placeholdDict['averageMoves'] = sum(spinList) / float(len(spinList))
			placeholdDict['hitChute'] = newChuteHit
			dataRecord.append(placeholdDict)
			print(placeholdDict)
#print("\n\n\n\n\n\n\n\n\n\n\n")

#print(dataRecord)


file = open('output.csv', 'w')

fields = ('startSpace', 'endSpace', 'averageMoves', 'hitChute')
wr = csv.DictWriter(file, fieldnames=fields, lineterminator = '\n')

wr.writeheader()
for bit in dataRecord:
        test = {'startSpace':bit['startSpace'], 'endSpace': bit['endSpace'], 'averageMoves':bit['averageMoves'], 'hitChute':bit['hitChute']}
        wr.writerow(test)
file.close()
