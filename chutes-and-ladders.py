"""
TITLE: CHUTES AND LADDERS SIMULATER
AUTHOR: ABHIMANYU DEORA
LICENSE: CREATIVE COMMONS ATTRIBUTION-NODERIVATIVES 4.0 INTERNATIONAL
DESCRIPTION: This is a program to simulate multiple games of chutes and ladders.
"""

import math
import random


spinList = [] #the # of spins for each game will be put in here
iterations = 0 #incrementer for the number of games
chutesList = [] #list for # of times a chute was hit per game
laddersList = [] #list for # of times a ladder was hit per game

while (iterations < 10000): #THE NUMBER OF GAMES PLAYED
    #SETTING UP VARIABLES FOR EACH GAME
    currentSpace = 0
    endSpace = 100
    spins = 0
    chutesHit = 0
    laddersHit = 0



    print("----------\n-----------\n-------\nIteration: %s -----------\n---------\n-------\n\n\n\n\n\n"%(iterations))
    while (currentSpace != 100): #LOOP FOR EACH GAME
        x = random.randint(1,6) #ROLLING
        
        if currentSpace + x <= 100:
            currentSpace += x
            
        if currentSpace == 1: #A LADDER
            currentSpace = 38
            laddersHit += 1
        if currentSpace == 4: #LADDER
            currentSpace = 14
            laddersHit += 1
        if currentSpace == 9: #LADDER
            currentSpace = 31
            laddersHit += 1
        if currentSpace == 21: #LADDER
            currentSpace = 42
            laddersHit += 1
        if currentSpace == 28: #LADDER
            currentSpace = 84
            laddersHit += 1
        if currentSpace == 36: #LADDER
            currentSpace = 44
            laddersHit += 1
        if currentSpace == 47: #CHUTE
            currentSpace = 26
            chutesHit += 1
        if currentSpace == 49: #CHUTE
            currentSpace = 11
            chutesHit += 1
        if currentSpace == 51: #CHUTE
            currentSpace = 67
            laddersHit += 1
        if currentSpace == 56: #CHUTE
            currentSpace = 53
            chutesHit += 1
        if currentSpace == 62: #CHUTE
            currentSpace = 19
            chutesHit += 1
        if currentSpace == 64: #CHUTE
            currentSpace = 60
            chutesHit += 1
        if currentSpace == 71: #LADDER
            currentSpace = 91
            laddersHit += 1
        if currentSpace == 80: #LADDER
            currentSpace = 100
            laddersHit += 1
        if currentSpace == 87: #CHUTE
            currentSpace = 24
            laddersHit += 1
        if currentSpace == 93: #CHUTE
            currentSpace = 73
            chutesHit += 1
        if currentSpace == 95: #CHUTE
            currentSpace = 75
            chutesHit += 1
        if currentSpace == 98: #CHUTE
            currentSpace = 78
            chutesHit += 1
        spins += 1
        print("Roll #: %s" %(spins))
    spinList.append(spins) #for this game, add the # of spins to the list
    chutesList.append(chutesHit) #same for chutes
    laddersList.append(laddersHit) #same for ladders
    iterations += 1 #increment the counter



print("\n\n\n\n") #PRINT RESULTS
print("-----------------")
print("Minimum number of moves: %s" %(min(spinList)))
print("Maximum number of moves: %s" %(max(spinList)))
print("Average Number of Spins to Win: %s"%(sum(spinList) / float(len(spinList))))
print("Average Number of Chutes Hit: %s"%(sum(chutesList) / float(len(chutesList))))
print("Average Number of Ladders Hit: %s"%(sum(laddersList) / float(len(laddersList))))
